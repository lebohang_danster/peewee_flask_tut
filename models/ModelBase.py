from peewee import Model, SqliteDatabase

database = SqliteDatabase("mysqlite.db", autocommit=True)


class ModelBase(Model):
    class Meta:
        database = database
