from peewee import CharField

from models.ModelBase import ModelBase


class UserModel(ModelBase):
    FirstName = CharField(250)
    LastName = CharField(250)

    class Meta:
        db_name = 'User'

if not UserModel.table_exists():
    UserModel.create_table()

