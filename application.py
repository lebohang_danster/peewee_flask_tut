from flask import Flask
from flask_restful import Api

from resources.User import User

app = Flask(__name__)
api = Api(app)


api.add_resource(User, '/api/user')

if __name__ == "__main__":
    app.run(debug=True)