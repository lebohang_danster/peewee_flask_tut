import jsonpickle as jsonpickle
from flask_restful import Resource

from models import UserModel


class User(Resource):

    def get(self):

        users = UserModel.select().dicts().get()
        return jsonpickle.dumps(users)



